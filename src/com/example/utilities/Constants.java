package com.example.utilities;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String DESCRIPTION = "Rock, Scissors, Paper Java implementation!\n";
    public static final String SELECTION_DESCRIPTION = "Choose operation(1, 2 or 3): ";
    public static final String OPTINS_TEXT = "Hi %s, choose option(r - > rock, s -> scissors, p -> paper, q -> quit): ";
    public static final String ENTER_NAME = "Enter name: ";

    public static final String TABLE_BORDER = "+-------+-----------------+--------------------------+----------|------------|%n";
    public static final String ALIGN_FORMAT =  "| %-5s | %-15s | %-24s | %-8s | %-10s |%n";
    public static final String COLUMNS = "| ID    | UserName        | DatePlayed               | You      | Oponent    |%n";

    public static final Map<String, String> MENU_ITEMS = new HashMap<>();
    static {
        MENU_ITEMS.put("1", ") Play Game!");
        MENU_ITEMS.put("2",") Statistics!");
        MENU_ITEMS.put("3", ") Exit!");
    }

    private Constants() {}
}
