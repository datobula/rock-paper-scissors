package com.example.utilities;

public class ErrorMessages {

    public static final String INVALID_OPERATION = "Invalid Operation!";
    public static final String LENGTH_ERROR = "Input is too long!";

    private ErrorMessages() {}
}
