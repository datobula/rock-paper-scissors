package com.example.statistic.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class User {

    private Integer id;
    private String name;
    private LocalDateTime startDate;
    private String status;
    private Integer score;
    private Integer oponentScore;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public Integer getScore() {
        return score;
    }

    public Integer getOponentScore() {
        return oponentScore;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setOponentScore(Integer oponentScore) {
        this.oponentScore = oponentScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(startDate, user.startDate) &&
                Objects.equals(status, user.status) &&
                Objects.equals(score, user.score) &&
                Objects.equals(oponentScore, user.oponentScore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, startDate, status, score, oponentScore);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startDate=" + startDate +
                ", status='" + status + '\'' +
                ", score='" + score + '\'' +
                ", oponentScore='" + oponentScore + '\'' +
                '}';
    }
}
