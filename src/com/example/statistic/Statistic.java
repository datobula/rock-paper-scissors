package com.example.statistic;

import com.example.statistic.model.User;

import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static com.example.utilities.Constants.ALIGN_FORMAT;
import static com.example.utilities.Constants.COLUMNS;
import static com.example.utilities.Constants.TABLE_BORDER;

public class Statistic {

    private static Set<User> users = new TreeSet<>(Comparator.comparing(User::getName));

    public void addRecord(User user) {
        users.add(user);
    }

    public Integer getLastRecordId() {
        return users
                .stream()
                .max((o1, o2) -> o2.getId() - o1.getId())
                .map(user1 -> user1.getId() + 1)
                .orElse(1);
    }

    public Optional<User> getUserBy(String name) {
        return users
                .stream()
                .filter(user -> user.getName().equals(name))
                .findFirst();
    }

    public void printUsersPretty() {
        System.out.format(TABLE_BORDER);
        System.out.format(COLUMNS);
        System.out.format(TABLE_BORDER);
        for (User user : users) {
            System.out.format(ALIGN_FORMAT,
                    user.getId(),
                    user.getName(),
                    user.getStartDate().toString(),
                    user.getScore(),
                    user.getOponentScore());
        }
        System.out.format(TABLE_BORDER);
    }
}
