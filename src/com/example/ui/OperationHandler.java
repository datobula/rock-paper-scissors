package com.example.ui;

@FunctionalInterface
public interface OperationHandler {

    void handle();
}
