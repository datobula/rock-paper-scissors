package com.example.ui;

import com.example.logic.LogicObjectsPool;
import com.example.logic.Options;
import com.example.logic.StrategiesFacade;
import com.example.statistic.Statistic;
import com.example.statistic.model.User;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;

import static com.example.utilities.Constants.ENTER_NAME;
import static com.example.utilities.Constants.OPTINS_TEXT;
import static com.example.utilities.ErrorMessages.INVALID_OPERATION;
import static com.example.utilities.ErrorMessages.LENGTH_ERROR;

public class PlayGame {

    private static final Integer DEFAULT_VALUES = 0;
    private static final String TERMINATOR = "q";
    private static final Random random = new Random();
    private static final Map<String, Options> powers = new HashMap<>();
    private static final Map<Integer, String> statuses = new HashMap<>();

    private Scanner scanner;
    private Statistic statistic;
    private LogicObjectsPool pool;
    private StrategiesFacade facade;


    static {
        powers.put("r", Options.ROCK);
        powers.put("s", Options.SCISSORS);
        powers.put("p", Options.PAPER);

        statuses.put(1, "sorry, computer wins !");
        statuses.put(0, "it's a tie!");
        statuses.put(-1, "congrats, you win!");
    }

    public PlayGame(LogicObjectsPool pool) {
        this.pool = pool;
        this.facade = pool.Facade;
        this.scanner = new Scanner(System.in);
        this.statistic = new Statistic();
        this.play();
    }

    private void play() {
        String userName = inputName();
        while (true) {
            String option = inputOption(userName);

            if(option.equals(TERMINATOR)) {
                break;
            }

            Options computer = computerOption();
            Options user = powers.get(option);

            pool.saveInHistories(user, computer);

            String status = result(computer, user);

            User record = getUser(userName, computer, user, status);

            statistic.addRecord(record);
        }
    }

    private User getUser(String userName, Options computer, Options user, String status) {
        User record;
        Optional<User> optional = statistic.getUserBy(userName);

        if(optional.isPresent()) {
            record = optional.get();
            if(user.compare(computer) > DEFAULT_VALUES) {
                record.setScore(record.getScore() + 1);
            } else if(user.compare(computer) < DEFAULT_VALUES) {
                record.setOponentScore(record.getOponentScore() + 1);
            }
        } else {
            record = new User();
            record.setId(statistic.getLastRecordId());
            record.setName(userName);
            record.setStartDate(LocalDateTime.now());
            record.setStatus(status);
            record.setScore(DEFAULT_VALUES);
            record.setOponentScore(DEFAULT_VALUES);
        }
        return record;
    }

    private String result(Options computer, Options user) {
        System.out.println(String.format("you choose  %s", user.name()));
        System.out.println(String.format("computer choose  %s", computer.name()));

        String status = statuses.get(computer.compare(user));
        System.out.println(status);

        return status;
    }

    private String inputName() {
        String name;
        while (true) {
            System.out.print(ENTER_NAME);
            name = scanner.next();
            if (name.length() <= 15) {
                break;
            }
            System.out.println(LENGTH_ERROR);
        }
        return name;
    }

    private String inputOption(String userName) {
        String option;
        while (true) {
            System.out.print(String.format(OPTINS_TEXT, userName));
            option = scanner.next();
            if (option.equals(TERMINATOR) || Objects.nonNull(powers.get(option))) {
                break;
            }
            System.out.println(INVALID_OPERATION);
        }

        return option;
    }

    public Options computerOption() {
        return this.facade.answer();
    }
}
