package com.example.ui;

import com.example.logic.LogicObjectsPool;
import com.example.statistic.Statistic;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

import static com.example.utilities.Constants.DESCRIPTION;
import static com.example.utilities.Constants.MENU_ITEMS;
import static com.example.utilities.Constants.SELECTION_DESCRIPTION;
import static com.example.utilities.ErrorMessages.INVALID_OPERATION;

public class Game {

    private Scanner scanner = new Scanner(System.in);

    private static final Map<String, OperationHandler> operationHandler = new HashMap<>();

    static {
        Statistic statistic = new Statistic();
        operationHandler.put("1", () -> new PlayGame(new LogicObjectsPool()));
        operationHandler.put("2", statistic::printUsersPretty);
        operationHandler.put("3", Game::exit);
    }

    private void showMenu() {
        System.out.println(DESCRIPTION);
        MENU_ITEMS.keySet().forEach(item -> {
            System.out.print(item);
            System.out.println(MENU_ITEMS.get(item));
        });
        System.out.print(SELECTION_DESCRIPTION);
    }

    public void playGame() {
        String operation;
        while (true) {
            showMenu();
            operation = scanner.next();
            if (Objects.isNull(MENU_ITEMS.get(operation))) {
                System.out.println(INVALID_OPERATION);
                continue;
            }
            operationHandler.get(operation).handle();
        }
    }

    public static void exit() {
        System.exit(0);
    }
}
