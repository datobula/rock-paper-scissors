package com.example.logic;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class UserOptionsHistoryStrategy implements History, AnswerStrategy {

    private Map<Integer, Integer> optionsCounts = new HashMap<>();
    private final Double wheelMinimalNumber;

    public UserOptionsHistoryStrategy(Options[] allOptions) {
        this(allOptions, 1d);
    }

    public UserOptionsHistoryStrategy(Options[] allOptions, Double wheelMinimalNumber) {
        Arrays.stream(allOptions).forEach(option -> {
            optionsCounts.put(option.ordinal(), 0);
        });

        this.wheelMinimalNumber = wheelMinimalNumber;
    }

    @Override
    public Options answer() {
        return RandomSelectionWheel.selectAny(
                optionsCounts
                        .entrySet()
                        .stream()
                        .map(entry -> new AbstractMap.SimpleEntry<>(Options.fromInteger(entry.getKey()), entry.getValue().doubleValue()))
                        .collect(Collectors.toList()),
                wheelMinimalNumber
        ).getWinnerOverThis();
    }

    @Override
    public void saveDecisions(Options userOption, Options computerOption) {
        optionsCounts.put(userOption.ordinal(), optionsCounts.get(userOption.ordinal()) + 1);
    }
}
