package com.example.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LogicObjectsPool {
    public final UserOptionsHistoryStrategy History_Strategy;
    public final LastActionStrategy Last_Action_Strategy;
    public final RandomAnswerStrategy Random_Strategy;
    public final StrategiesFacade Facade;
    public final List<History> All_Histories;

    public LogicObjectsPool() {
        History_Strategy = new UserOptionsHistoryStrategy(Options.values());
        Last_Action_Strategy = new LastActionStrategy();
        Random_Strategy = new RandomAnswerStrategy();

        List<AnswerStrategy> list = new ArrayList<AnswerStrategy>();
        list.add(History_Strategy);
        list.add(Last_Action_Strategy);
        list.add(Random_Strategy);

        Facade = new StrategiesFacade(list);

        ArrayList<History> historiesList = new ArrayList<>();
        historiesList.add(History_Strategy);
        historiesList.add(Last_Action_Strategy);
        historiesList.add(Facade);

        All_Histories = Collections.unmodifiableList(historiesList);
    }

    public void saveInHistories(Options userOption, Options computerOption) {
        All_Histories.forEach(history -> history.saveDecisions(userOption, computerOption));
    }
}
