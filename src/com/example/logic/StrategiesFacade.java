package com.example.logic;

import java.util.*;
import java.util.stream.Collectors;

public class StrategiesFacade implements AnswerStrategy, History{
    Map<AnswerStrategy, Integer> strategyWeights;
    List<Map.Entry<AnswerStrategy, Options>> lastAnswers;
    final int Win_Award = 3;
    final int Draw_Award = 1;

    public StrategiesFacade(List<AnswerStrategy> strategies) {
        this.strategyWeights = strategies
                .stream()
                .map(strategy -> new AbstractMap.SimpleEntry<>(strategy, 1))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public Options answer() {
        Map<AnswerStrategy, Options> answersMap = strategyWeights
                .entrySet()
                .stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), entry.getKey().answer()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));



        lastAnswers = new ArrayList<>(answersMap.entrySet());

        return Options.fromInteger(strategyWeights
                .entrySet()
                .stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(answersMap.get(entry.getKey()), entry.getValue()))
                .collect(Collectors.groupingBy(entry -> entry.getKey().ordinal(), Collectors.summingInt(entry -> entry.getValue())))
                .entrySet()
                .stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .get()
                .getKey()
        );
    }

    @Override
    public void saveDecisions(Options userOption, Options computerOption) {
        if(lastAnswers == null) return;

        // add weights according to football scoring
        lastAnswers.forEach(entry -> {
            if (entry.getValue().equals(userOption.getWinnerOverThis())) {
                strategyWeights.put(entry.getKey(), strategyWeights.get(entry.getKey()) + Win_Award);
            } else if (entry.getValue().equals(userOption)) {
                strategyWeights.put(entry.getKey(), strategyWeights.get(entry.getKey()) + Draw_Award);
            }
        });
    }
}
