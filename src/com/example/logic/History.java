package com.example.logic;

public interface History {
    void saveDecisions(Options userOption, Options computerOption);
}
