package com.example.logic;

import java.util.Random;

public class RandomAnswerStrategy implements AnswerStrategy {
    private Random random = new Random();

    @Override
    public Options answer() {
        return Options.fromInteger(random.nextInt(3));
    }
}
