package com.example.logic;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LastActionStrategy implements History, AnswerStrategy {
    private Integer[][] winActionsCounts = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    private Integer[][] loseActionsCounts = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    private Integer[][] drawActionsCounts = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    private Options lastUserOption;
    private Options lastComputerOption;
    private final Double wheelMinimalNumber;

    public LastActionStrategy() {
        this(1d);
    }

    public LastActionStrategy(Double wheelMinimalNumber) {
        this.wheelMinimalNumber = wheelMinimalNumber;
    }

    @Override
    public Options answer() {
        Integer[] counts;

        if (lastComputerOption == null || lastUserOption == null) {
            counts = new Integer[3];
            counts[0] = 0;
            counts[1] = 0;
            counts[2] = 0;
        } else {
            Integer[][] actionsCounts = this.getActionsCountsForGameResult(this.lastUserOption, this.lastComputerOption);

            counts = actionsCounts[this.lastUserOption.ordinal()];
        }

        List<Map.Entry<Options, Double>> entries = new ArrayList<>();
        entries.add(new AbstractMap.SimpleEntry<>(Options.fromInteger(0), counts[0].doubleValue()));
        entries.add(new AbstractMap.SimpleEntry<>(Options.fromInteger(1), counts[1].doubleValue()));
        entries.add(new AbstractMap.SimpleEntry<>(Options.fromInteger(2), counts[2].doubleValue()));

        return RandomSelectionWheel.selectAny(entries, wheelMinimalNumber).getWinnerOverThis();
    }

    private Integer[][] getActionsCountsForGameResult(Options userOption, Options computerOption) {
        Integer result = userOption.compare(computerOption);

        switch (result) {
            case 1: return winActionsCounts;
            case -1: return loseActionsCounts;
            case 0: return drawActionsCounts;
        }

        return drawActionsCounts; // will not come here
    }

    @Override
    public void saveDecisions(Options userOption, Options computerOption) {
        if (lastComputerOption == null || lastUserOption == null) {
            this.lastUserOption = userOption;
            this.lastComputerOption = computerOption;

            return;
        }

        Integer[][] actionsCounts = this.getActionsCountsForGameResult(userOption, computerOption);

        actionsCounts[lastUserOption.ordinal()][userOption.ordinal()]++;

        this.lastUserOption = userOption;
        this.lastComputerOption = computerOption;
    }
}
