package com.example.logic;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomSelectionWheel {

    private static final Random random = new Random();

    private static <T>  List<Map.Entry<T, Double>> numbersToNonZeroWheelParts(List<Map.Entry<T, Double>> entries, Double partForZero) {
        return entries
                .stream()
                .map(entry -> new AbstractMap.SimpleEntry<T, Double>(entry.getKey(), entry.getValue() + partForZero))
                .collect(Collectors.toList());
    }

    static <T> T selectFromNonZeroes(List<Map.Entry<T, Double>> entries) {
        Double sum = entries.stream().map(Map.Entry::getValue).reduce(0d, (acc, current) -> acc + current);

        Double wheelPointerNumber = random.nextDouble() * sum;
        Integer i;

        for (i = 0; wheelPointerNumber > 0; i++) {
            wheelPointerNumber -= entries.get(i).getValue();
        }

        return entries.get(i - 1).getKey();
    }

    static <T> T selectAny(List<Map.Entry<T, Double>> entries, Double partForZero) {
        return selectFromNonZeroes(numbersToNonZeroWheelParts(entries, partForZero));
    }
}
