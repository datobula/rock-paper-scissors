package com.example.logic;

public enum Options {

    ROCK(0),
    SCISSORS(1),
    PAPER(2);

    private int id;

    Options(int id) {
        this.id = id;
    }

    private static int[][] gameMap = {{0, 1, -1}, {-1, 0, 1}, {1, -1, 0}};
    private static final Options[] winnersMap = {PAPER, ROCK, SCISSORS};
    private static Options[] optionsMap = {ROCK, SCISSORS, PAPER};

    public int compare(Options option) {
        return gameMap[this.id][option.id];
    }

    public Options getWinnerOverThis() {
        return winnersMap[this.id];
    }

    public static Options fromInteger(Integer number) {
        return optionsMap[number];
    }
}
