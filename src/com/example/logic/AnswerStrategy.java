package com.example.logic;

public interface AnswerStrategy {
    Options answer();
}
